﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12._08_Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            //DateTime dt = new DateTime();
            //DateTime dt2 = new DateTime();
            //dt.CompareTo(dt2);

            Person p1 = new Person(5555, 23, "Jack", 1.68f);
            Person p2 = new Person(3333, 32, "John", 1.91f);
            Person p3 = new Person(4444, 45, "William", 1.75f);
            Person p4 = new Person(2222, 62, "Tom", 1.88f);
            Person p5 = new Person(1111, 81, "Roy", 1.83f);

            Person[] persons = { p1, p3, p2, p4, p5 };

            PrintPersonArray(persons);

            
            Console.WriteLine("Sorted by ID=================");
            Array.Sort(persons, Person.IDComparer);
            PrintPersonArray(persons);

            Console.WriteLine("Sorted by Name===============");
            Array.Sort(persons, Person.NameComparer);
            PrintPersonArray(persons);

            Console.WriteLine("Sorted by Height===============");
            Array.Sort(persons, Person.HeightComparer);
            PrintPersonArray(persons);

            Console.WriteLine("Sorted by Age===============");
            Array.Sort(persons, Person.AgeComparer);
            PrintPersonArray(persons);

        }
        public static void PrintPersonArray(Person[]persons)
        {
            foreach (Person p in persons)
            {
                Console.WriteLine(p);
            }
        }

    }
}
