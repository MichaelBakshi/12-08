﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace _12._08_Homework
{
    class Person : IComparable<Person>
    {
        public int Id { get; private set; }
        public int Age { get; private set; }
        public float Height { get; private set; }
        public string Name { get; private set; }

        private static readonly IComparer<Person> idComparer;
        private static readonly IComparer<Person> ageComparer;
        private static readonly IComparer<Person> heightComparer ;
        private static readonly IComparer<Person> nameComparer ;
        public static IComparer<Person> IDComparer { get{ return idComparer; } }
        public static IComparer<Person> AgeComparer { get { return ageComparer; } }
        public static IComparer<Person> HeightComparer { get { return heightComparer; } }
        public static IComparer<Person> NameComparer { get { return nameComparer; } }

        private static IComparer<Person> defaultComparer;

        public Person(int id, int age, string name, float height)
        {
            this.Id = id;
            this.Age = age;
            this.Name = name;
            this.Height = height;
        }
        static Person()
        {
            idComparer = new PersonCompareById();
            ageComparer = new PersonCompareByAge();
            heightComparer = new PersonCompareByHeight();
            nameComparer = new PersonCompareByName();
        }
        public static void ModifyDefaultComparer(string modifyComparer)
        {

        }

        public int CompareTo(Person p)
        {
            return this.Id - p.Id;
        }

        public static IComparer<Person> GetDefaultComparer()
        {
            return new PersonCompareById();
        }
        public override string ToString()
        {
            return $"Name: {Name} Id: {Id} Age: {Age} Height: {Height}";
        }
    }
}
